"use strict";

document.addEventListener('DOMContentLoaded', function () {

    let checkbox1 = document.getElementById('checkbox1'),
        email = document.getElementById('emailText'),
        phonenumber = document.getElementById('phonenumberText'),
        message = document.getElementById('messageText'),
        popupButton = document.getElementById("popupButton"),
        popupOverlay = document.getElementById('popupOverlay'),
        popupform = document.getElementById('popupForm'),
        cross = document.getElementById('cross');
        
    cross.addEventListener('click', function (event) {
        popupOverlay.style.display = 'none';
        popupform.style.display = 'none';
    })

    window.onpopstate = function () {
        popupOverlay.style.display = 'none';
        popupform.style.display = 'none';
    }

    if (localStorage.length == 0) {
        localStorage.setItem('emailText', '');
        localStorage.setItem('phonenumberText', '');
        localStorage.setItem('messageText', '');
        localStorage.setItem('checkbox1status', checkbox1.value);
    } else {
        email.value = localStorage.getItem('emailText');
        phonenumber.value = localStorage.getItem('phonenumberText');
        message.value = localStorage.getItem('messageText');
        checkbox1.value = localStorage.getItem('checkbox1status');
    }

  
    popupButton.addEventListener('click', function (event) {
        popupOverlay.style.display = 'block';
        popupform.style.display = 'block';
        history.pushState(null, '', "form.html");
    });
    email.addEventListener('change', function (event) {
        localStorage.setItem('emailText', email.value);
    });
    phonenumber.addEventListener('change', function (event) {
        localStorage.setItem('phonenumberText', phonenumber.value);
    });
    message.addEventListener('change', function (event) {
        localStorage.setItem('messageText', message.value);
    });
    checkbox1.addEventListener('change', function (event) {
        localStorage.setItem('checkbox1status', checkbox1.value);
    });
});