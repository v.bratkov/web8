$(document).ready(function () {
   let popupOverlay = document.getElementById('popupOverlay'),
    popupform = document.getElementById('popupForm');
    $(".ajaxForm").submit(function(e){

        e.preventDefault();

        var href = $(this).attr("action");

        $.ajax({

            type: "POST",

            dataType: "json",

            url: href,

            data: $(this).serialize(),

            success: function(response){

                if(response.status == "success"){

                    alert("We received your submission, thank you!");
                    localStorage.clear();
                    popupOverlay.style.display = 'none';
                    popupform.style.display = 'none';
                    document.getElementById('myForm').reset();

                }else{

                    alert("An error occured: " + response.message);

                }

            }

        });

    });

});